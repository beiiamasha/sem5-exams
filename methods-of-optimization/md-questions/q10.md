# 10. Виды задач линейного программирования (ЛП).

**Общего вида**:
```math
\begin{cases}
    \sum\limits_{j=1}^{n} c_j x_j \to max \\
    \sum\limits_{j=1}^{n} a_{ij} x_j \le b_i, \space i=\overline{1,m_1} \\
    \sum\limits_{j=1}^{n} a_{ij} x_j = b_i, \space i=\overline{m_1+1,m_2} \\
    \sum\limits_{j=1}^{n} a_{ij} x_j \ge b_i, \space i=\overline{m_2+1,m}
\end{cases}
```

1. Неотрицательных переменных:
```math
\begin{cases}
    \sum\limits_{j=1}^{n} c_j x_j \to max \\
    \sum\limits_{j=1}^{n} a_{ij} x_j \le b_i, \space i=\overline{1,m_1} \\
    \sum\limits_{j=1}^{n} a_{ij} x_j = b_i, \space i=\overline{m_1+1,m} \\
    x_j \ge 0, \space j=\overline{1,n}
\end{cases}
```

2. Стандартная форма:
```math
\begin{cases}
    \sum\limits_{j=1}^{n} c_j x_j \to max \\
    \sum\limits_{j=1}^{n} a_{ij} x_j \le b_i, \space i=\overline{1,m}\\
    x_j \ge 0, \space j=\overline{1,n}
\end{cases}
```

3. Каноническая форма:
```math
\begin{cases}
    \sum\limits_{j=1}^{n} c_j x_j \to max \\
    \sum\limits_{j=1}^{n} a_{ij} x_j = b_i, \space i=\overline{1,m}\\
    x_j \ge 0, \space j=\overline{1,n}
\end{cases}
```

4. Матричная стандартная форма:
```math
\begin{cases}
    (c,x) \to max \\
    Ax \le b \\
    x \ge 0
\end{cases}
```