# Билеты по методам оптимизации, матстату и матлингвистике

### Для использования репозитория:
1. Установить VSCode.
2. Установить расширение *Markdown+Math* от *goessner*.
3. Зайти в *Settings* (**Ctrl+,**), перейти к вкладке *Extensions*, затем найти там раздел *mdmath*. В поле *Delimiters* вместо `dollars` написать `gitlab`.
4. Перезагрузить VSCode.

### Для конвертирования в pdf:
1. Зайти в *Command Palette* (**Ctrl+Shift+P**)
2. Выполнить *Save Markdown+Math to HTML*.
3. Преобразовать сгенерированный html в pdf любым предпочитаемым образом. Самый наивный - открыть в браузере и выбрать *Сохранить страницу как*.

### Маленькая справка по markdown и latex
Справка по [markdown](https://paulradzkov.com/2014/markdown_cheatsheet/).  
Справка по [latex](http://archive.li/JMh16).

Справка по [katex](https://katex.org/docs/supported.html).

Gitlab отображает markdown в стиле `gitlab`, поэтому есть ряд отличий:
1. Строчные формулы вставляются не с помощью знаков $$, а вот такой конструкцией: $`y=x^2`$
2. Большие формулы оформляются, как будто это код:
```math
F = \frac
    {\alpha X}
    {\beta Y + \gamma Z}
```